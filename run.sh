command_exists () {
  type "$1" &> /dev/null ;
}

if [ -n "$DBG_SH_EXECUTE_URL" ] 
then
  debug "Debug URL defined. Loading script into context ..."

  # getting together necessary dependencies and directories
  debug "INIT: checking existance of dependencies and directories"
  if ! ( command_exists curl ); then
    debug " - curl does not exist. Trying to apt-get ..."
    if command_exists apt-get; then
      apt-get install -y curl
    fi
    if ! ( command_exists curl ); then
      debug " - failed installing curl - aborting ..."
      exit 1
    fi
  fi

  # loading script from src
  curl $DBG_SH_EXECUTE_URL > $WERCKER_CACHE_DIR/dbg.sh
  chmod +x $WERCKER_CACHE_DIR/dbg.sh
  debug "Successfully loaded following script:"
  cat $WERCKER_CACHE_DIR/dbg.sh
  source $WERCKER_CACHE_DIR/dbg.sh
fi