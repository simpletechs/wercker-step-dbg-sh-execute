# dbg-sh-execute

Small step to allow execution of sh-scripts from remote for debugging purposes.

# Usage

Define an environment variable `DBG_SH_EXECUTE_URL` containing the URL to the sh-script and include this step in your build or deploy steps.

# Changelog

## 0.1.0

- Initial release